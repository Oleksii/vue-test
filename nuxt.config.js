import colors from "vuetify/es5/util/colors";

module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "vue-test",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Test project with nuxt"
      }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  modules: ["@nuxtjs/axios"],
  devModules: ["@nuxtjs/vuetify"],
  vuetify: {
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.deepPurple.lighten1,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.darken1,
          success: colors.teal.darken3
        }
      }
    }
  },
  axios: {
    baseURL: "https://api.scryfall.com"
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3B8070" },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    }
  }
};
