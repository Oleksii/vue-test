export const state = () => ({
  isLoading: false,
  data: []
});

export const mutations = {
  setLoading(state, value) {
    state.isLoading = value;
  },

  setData(state, value) {
    state.data = value;
  }
};

export const actions = {
  async searchCards({ state, commit }, queryString) {
    commit("setData", []);
    if (!queryString) return;
    commit("setLoading", true);
    try {
      const {
        data: { data }
      } = await this.$axios.get("/cards/search", {
        params: { q: queryString }
      });
      commit("setData", data);
    } catch (e) {
      console.error(e);
      alert("Something went wrong");
    } finally {
      commit("setLoading", false);
    }
  }
};
